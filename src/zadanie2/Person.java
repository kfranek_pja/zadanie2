package zadanie2;

import java.util.ArrayList;
import java.util.Dictionary;

public class Person {
	private Dictionary<String, Address> addresses;
	private Dictionary<String, String> phoneNumbers;
	private ArrayList<Role> roles;
	private String firstname;
	private String lastname;
	
	public Person(Dictionary<String, Address> addresses, ArrayList<Role> roles){
		this.addresses = addresses;
		this.roles = roles;
	}
	
	public void setAddress(Dictionary<String, Address> addresses){
		this.addresses = addresses;
	}
	
	public Dictionary<String, Address> getAddresses(){
		return this.addresses;
	}
	
	public void setRoles(ArrayList<Role> roles){
		this.roles = roles;
	}
	
	public ArrayList<Role> getRoles(){
		return this.roles;
	}
	
	public void setFirstname(String firstname){
		this.firstname = firstname;
	}
	
	public String getFirstname(){
		return this.firstname;
	}
	
	public void setLastname(String lastname){
		this.lastname = lastname;
	}
	
	public String getLastname(){
		return this.lastname;
	}
	
	public void setPhoneNumbers(Dictionary<String, String> phoneNumbers){
		this.phoneNumbers = phoneNumbers;
	}
	
	public Dictionary<String, String> getPhoneNumbers(){
		return this.phoneNumbers;
	}
}
