package zadanie2;


public class Permission {
	private String name = "";
	private boolean status = true;
	
	public Permission(String name, boolean status){
		this.name = name;
		this.status = status;
	}
	
	public void setStatus(boolean status){
		this.status = status;
	}
	
	public boolean getStatus(){
		return this.status;
	}
	
	public String getName(){
		return this.name;
	}
}
