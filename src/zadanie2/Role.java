package zadanie2;

import java.util.ArrayList;


public class Role {
	private String name = "";
	private ArrayList<Permission> permissions = new ArrayList<Permission>();
	
	public Role(String name, ArrayList<Permission> roles, ArrayList<Permission> permissions){
		this.name = name;
		this.permissions = permissions;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setRoles(ArrayList<Permission> permissions){
		this.permissions = permissions;
	}
	
	public ArrayList<Permission> getRoles(){
		return this.permissions;
	}
}
