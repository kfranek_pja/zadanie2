package zadanie2;

public class Address {
	private String city = "";
	private String street = "";
	private String zipcode = "";
	private String country = "";
	private String building = "";
	
	public Address(String country, String city, String street, String building, String zipcode){
		this.country = country;
		this.street = street;
		this.city = city;
		this.building = building;
		this.zipcode = zipcode;
	}
	
	public void setCountry(String country){
		this.country = country;
	}
	
	public void setCity(String city){
		this.city = city;
	}
	
	public void setStreet(String street){
		this.street = street;
	}
	
	public void setBuilding(String building){
		this.building = building;
	}
	
	public void setZipcode(String zipcode){
		this.zipcode = zipcode;
	}
	
	public String getCountry(){
		return this.country;
	}
	
	public String getStreet(){
		return this.street;
	}
	
	public String getCity(){
		return this.city;
	}
	
	public String getBuilding(){
		return this.building;
	}
	
	public String getZipcode(){
		return this.zipcode;
	}
	
}
